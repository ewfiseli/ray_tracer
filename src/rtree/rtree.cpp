/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rtree/rtree.h"
#include "_rtree.h"
#include "objects/object.h"
#include "math/bounds3d.h"
#include "core/ray.h"
#include "core/intersection.h"


RTree::RTree() : impl_( new RTree_Detail::RTreeImpl() ) { }

RTree::~RTree()
{
    if (impl_)
		delete impl_;
}

void RTree::Insert(const Object *object)
{
	impl_->Insert(object);
}

Intersection* RTree::Search(const Ray &ray) const
{
	return impl_->Search(ray);
}

Intersection* RTree::Search(const Ray &ray, float max) const
{
    return impl_->Search(ray, max);
}


