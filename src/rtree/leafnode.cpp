/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "_rtreenode.h"
#include "_rtree.h"
#include "rtree/rtree.h"

#include <cassert>

namespace RTree_Detail {

LeafNode::LeafNode() : TreeNode(), objects_(RTree::max_children) { }

LeafNode::~LeafNode() { }

void LeafNode::AddChild(const Object* object)
{
    assert(size_ < RTree::max_children && size_ >= 0);
    assert(object && object->BoundingBox());

    bounding_boxes_[size_] = object->BoundingBox();
    objects_[size_] =  object;

    ++size_;
    this->Tighten();
}

void LeafNode::Tighten()
{
	assert(size_ >= 1);
	
	node_bounds_.CopyBounds(*bounding_boxes_[0]);
	
	for (int i=1; i < size_; ++i) {
		assert(bounding_boxes_[i]);
		node_bounds_.Enlarge(*bounding_boxes_[i]);
	}
}

const Object* LeafNode::GetChild(int i) const
{
    assert(i >= 0 && i < size_);
    return objects_[i];
}

bool LeafNode::IsLeaf() const
{
    return true;
}

void LeafNode::AcceptVisit(RTreeVisitor &visitor) const 
{ 
	visitor.Visit(this); 
}

/*
void LeafNode::AcceptVisit(RTreeVisitor &visitor) 
{ 
	visitor.Visit(this); 
}
*/

} /* namespace RTree_Detail */
