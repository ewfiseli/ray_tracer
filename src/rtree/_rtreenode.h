/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTREENODE_PRIVATE_H
#define RTREENODE_PRIVATE_H

#include "math/bounds3d.h"
#include "objects/object.h"
#include <vector>

namespace RTree_Detail {
	
class RTreeVisitor;
	
class TreeNode
{
public:
    /* Parent of the node, Nodes bounding box and max and min branch factor */
    TreeNode();

    /* Destructor */
    virtual ~TreeNode();

    /* Recalculate the min bounding box for the node */
    virtual void Tighten();

    /* Returns true if it is a leaf node, bad practice, i know */
    virtual bool IsLeaf() const;
	
	/* use the visitor for searching and other traversing */
	virtual void AcceptVisit(RTreeVisitor &visitor) const;
	//virtual void AcceptVisit(RTreeVisitor &visitor);

    /* returns true if this node has reached max branch factor */
    bool IsFull() const;

    /* Returns the number of children */
    int Size() const;

    /* Gets a child at index i */
    TreeNode* GetChild(int i) const;

    /* Add a child to the node, increases size by one */
    void AddChild(TreeNode* node);

    /* Removes and returns the child at index i, size decrements */
    //TreeNode* RemoveChild(int i);

    /* Get the bounding box for current node */
    const Math::BoundingBox3D* BoundingBox() const;
	Math::BoundingBox3D* BoundingBox();

    /* Get the child at i bounding box */
    const Math::BoundingBox3D* ChildBoundingBox(int i) const;

    /* Set the parent node for this node */
    void SetParent(TreeNode* parent);

    /* Get the Nodes Parent Node */
    TreeNode* GetParent() const;

    TreeNode* parent_;

    /* Bounding Box for Node */
    Math::BoundingBox3D node_bounds_;

    /* max branch factor, min branch factor, and current size */
    int size_;

    /* Entry Variables */
	std::vector<TreeNode*> children_;
	std::vector<const Math::BoundingBox3D*> bounding_boxes_;
};

class LeafNode : public TreeNode
{
public:
    LeafNode();
    ~LeafNode();

    void AddChild(const Object* object);
    const Object* GetChild(int i) const;
	
	void Tighten();
    bool IsLeaf() const;
	
	/* use the visitor for searching and other traversing */
	void AcceptVisit(RTreeVisitor &visitor) const;
	//void AcceptVisit(RTreeVisitor &visitor);

    std::vector<const Object*> objects_;
};

} /* namespace RTree_Detail */
#endif /* RTREENODE_PRIVATE_H */