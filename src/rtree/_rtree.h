/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTREE_PRIVATE_H
#define RTREE_PRIVATE_H

#include "rtree/rtree.h"
#include "_rtreenode.h"
#include "math/bounds3d.h"

#include <vector>

class Object;
class Ray;
class Intersection;

namespace RTree_Detail {
	
class RTreeSearchHelper;

	
class RTreeImpl 
{
public:
	RTreeImpl();
	~RTreeImpl();
	
	void Insert(const Object *object);
	
	Intersection *Search(const Ray &ray) const;
	Intersection *Search(const Ray &ray, float max) const;
	
	LeafNode *ChooseLeaf(const Math::BoundingBox3D &box);
	
	void AdjustTree(TreeNode *L, TreeNode *LL=nullptr);
private:
	TreeNode *root_;
	int num_items_;
};


class RTreeVisitor 
{
public:
	virtual ~RTreeVisitor() { }
	virtual void Visit(const LeafNode *node) = 0;
	virtual void Visit(const TreeNode *node) = 0;
};


class RTreeSearchVisitor : public RTreeVisitor 
{
public:
	RTreeSearchVisitor(const Ray &ray);
	
	void Visit(const LeafNode *leaf);
	
	void Visit(const TreeNode *node);
	
	inline Intersection *GetResult() {
		return intersect_;
	}
    
    float min_dist;
private:
	const Ray &ray_;
	Intersection *intersect_, *tmp_intersect_;
	DISALLOW_COPY_AND_ASSIGN(RTreeSearchVisitor);
};


void LinearPickSeeds(std::vector<const Math::BoundingBox3D*> &boxes,
					 int &index1, int &index2);


template <typename NodeT, typename ExtraT>
NodeT *SplitNode(NodeT *node, ExtraT *extra_item) 
{
    assert(node->IsFull());
	
	/* Create new node that results from split */
    NodeT* new_node = new NodeT();
    std::vector<ExtraT*> node_elements(RTree::max_children + 1);
	
	std::vector<const Math::BoundingBox3D*> box_elements(RTree::max_children + 1);
    for (int i=0; i < RTree::max_children; ++i) {
        box_elements[i] = node->bounding_boxes_[i];
        assert(box_elements[i]);

        node_elements[i] = node->GetChild(i);
        assert(node_elements[i]);
    }
    
    node->size_ = 0;

    node_elements[RTree::max_children] = extra_item;
    box_elements[RTree::max_children] = extra_item->BoundingBox();

    /* Invoke Linear Pick Seeds */
    int elem_1, elem_2;
    LinearPickSeeds(box_elements, elem_1, elem_2);

    /* Assign One seed to the current box, and one seed to the new box */
    new_node->AddChild(node_elements[elem_1]);
    node->AddChild(node_elements[elem_2]);

    float temp_expan_1, temp_expan_2, temp_size_1, temp_size_2;
    int i = 0;
    int left = RTree::max_children - 1;
    /* Add Remaining elements to one of the two nodes */
    while (i < RTree::max_children + 1) {
        if (i == elem_1 || i == elem_2) {
            i++;
        }
        else if (node->Size() < RTree::min_children && 
					RTree::min_children - left == 0) {
            node->AddChild(node_elements[i]);
            left--;
            ++i;
        }
        else if (new_node->Size() < RTree::min_children && 
						RTree::min_children - left == 0) {
            new_node->AddChild(node_elements[i]);
            left--;
            ++i;
        } else {
            temp_expan_1 = node->node_bounds_.RequiredEnlargement(
								*node_elements[i]->BoundingBox());
			
            temp_size_1 = node->node_bounds_.Volume() + temp_expan_1;

            temp_expan_2 = new_node->node_bounds_.RequiredEnlargement(
									*node_elements[i]->BoundingBox());
			
            temp_size_2 = new_node->node_bounds_.Volume() + temp_expan_2;

            /* Check Equality */
            if (std::abs(temp_expan_1 - temp_expan_2) <= 0.01f) {
                /* Compare resulting size */
                if (temp_size_1 < temp_size_2) {
                    node->AddChild(node_elements[i]);
                } else {
                    new_node->AddChild(node_elements[i]);
                }

            }
            /* Otherwise add to smallest resulting bounding box */
            else if (temp_expan_1 < temp_expan_2) {
                node->AddChild(node_elements[i]);
            }
            else {
                new_node->AddChild(node_elements[i]);
            }

            ++i;
            --left;
        }
    }

    return new_node;
}


} /* namespace RTree_Detail */
#endif /* RTREE_PRIVATE_H */