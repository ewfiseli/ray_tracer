/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "_rtreenode.h"
#include "rtree/rtree.h"
#include "_rtree.h"

#include <cassert>
#include <cmath>

namespace RTree_Detail {

TreeNode::TreeNode() : children_(RTree::max_children),
						bounding_boxes_(RTree::max_children)
{
       parent_ = nullptr;
	   for (int i=0; i < RTree::max_children; ++i) {
			children_[i] = nullptr;
	   }
       size_ = 0;
}

TreeNode::~TreeNode()
{
	for(int i=0; i < size_; ++i) {
		if (children_[i]) {
			delete children_[i];
			children_[i] = nullptr;
		}
	}
}

void TreeNode::Tighten()
{
    assert(size_ > 0);
	node_bounds_.CopyBounds(*bounding_boxes_[0]);	

    for (int i=1; i < size_; ++i) {
        assert(bounding_boxes_[i] && children_[i]);
		/* recursivly tighten, then expand this box */
		children_[i]->Tighten();
        node_bounds_.Enlarge(*bounding_boxes_[i]);
    }
}

bool TreeNode::IsLeaf() const
{
    return false;
}

void TreeNode::AcceptVisit(RTreeVisitor &visitor) const
{
	visitor.Visit(this);
}

bool TreeNode::IsFull() const
{
    return size_ == RTree::max_children;
}

int TreeNode::Size() const
{
    return this->size_;
}

TreeNode* TreeNode::GetChild(int i) const
{
    assert(!IsLeaf());
    return children_[i];
}

void TreeNode::AddChild(TreeNode* node)
{
    assert(size_ < RTree::max_children);
    assert(!IsLeaf());
	assert(node->BoundingBox());

    node->Tighten();
	
    children_[size_]   = node;
    bounding_boxes_[size_] =  node->BoundingBox();
    node->SetParent(this);
    ++size_;
	
    this->Tighten();
}

Math::BoundingBox3D* TreeNode::BoundingBox()
{
    return &node_bounds_;
}

const Math::BoundingBox3D *TreeNode::BoundingBox() const
{
	return &node_bounds_;
}

const Math::BoundingBox3D* TreeNode::ChildBoundingBox(int i) const
{
    return bounding_boxes_[i];
}

void TreeNode::SetParent(TreeNode* parent)
{
    parent_ = parent;
}

TreeNode* TreeNode::GetParent() const
{
    return parent_;
}

} /* namespace RTree_Detail */