/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "_rtree.h"
#include "_rtreenode.h"
#include "rtree/rtree.h"
#include "core/ray.h"
#include "objects/object.h"
#include "math/bounds3d.h"

#include <cmath>

namespace RTree_Detail {
	
RTreeImpl::RTreeImpl() : root_(new LeafNode())
{
	num_items_ = 0;
}

RTreeImpl::~RTreeImpl() 
{
	if (root_)
		delete root_;
}

void RTreeImpl::Insert(const Object* object)
{
    LeafNode* node = ChooseLeaf(*object->BoundingBox());

    if (node->IsFull()) {
        LeafNode* other_node = RTree_Detail::SplitNode<LeafNode, const Object>(node, object);
        AdjustTree(node, other_node);
    } else {
        node->AddChild(object);
        AdjustTree(node);
    }
}

Intersection* RTreeImpl::Search(const Ray &ray) const
{
	RTreeSearchVisitor visitor(ray);
	root_->AcceptVisit(visitor);
    return visitor.GetResult();
}

Intersection* RTreeImpl::Search(const Ray &ray, float max) const
{
    RTreeSearchVisitor visitor(ray);
    visitor.min_dist = max + (- EPSILON);
	root_->AcceptVisit(visitor);
    return visitor.GetResult();
}


void RTreeImpl::AdjustTree(TreeNode *L, TreeNode *LL)
{
    TreeNode *P, *PP;
    while (L != root_) {
        P = L->GetParent();
        L->Tighten();
        P->Tighten();
        if (LL) {
            LL->Tighten();
            if (P->IsFull()) {
                PP = SplitNode<TreeNode, TreeNode>(P, LL);
                LL = PP;
            } else {
                P->AddChild(LL);
                LL = nullptr;
            }
        }
        L = P;
    }

    /* Root has been split */
    /* ASSUMES MIN SIZE OF 2 */
    if (LL) {
        root_ = new TreeNode();
        root_->AddChild(L);
        root_->AddChild(LL);
    }
}

LeafNode* RTreeImpl::ChooseLeaf(const Math::BoundingBox3D& box)
{
    TreeNode* node = root_;

    int size, min_index;
    float min_enlargement, min_size, temp_enlargement, temp_size;
	
     /* All Tree Branches are terminated with leaf nodes so
      * So termination is garenteed
      */
     while (true) {
         if (node->IsLeaf())
            return reinterpret_cast<LeafNode*>(node);


         /* otherwise, Look for the rectangle that needs the least enlargement
          * and is the smallest
          */
         size = node->Size();
         assert(size > 0);
		 min_index = 0;
		 min_enlargement = node->ChildBoundingBox(0)->RequiredEnlargement(box);
		 min_size = node->ChildBoundingBox(0)->Volume() + min_enlargement;
         for (int i=1; i < size; ++i) {

             temp_enlargement = node->ChildBoundingBox(i)->RequiredEnlargement(box);
             temp_size = node->ChildBoundingBox(i)->Volume() + temp_enlargement;

             if ((std::abs(temp_enlargement - min_enlargement) <= 0.5)) {
				 if (temp_size < min_size) {
					min_index = i;
					min_enlargement = temp_enlargement;
					min_size = temp_size;
				 }
            } 
            else if (temp_enlargement < min_enlargement) {
				min_index = i;
				min_enlargement = temp_enlargement;
				min_size = temp_size;
			}

         } // END for
         node = node->GetChild(min_index);
         node->BoundingBox()->Enlarge(box);
    }

}


/* Assumes Size of boxes is RTree::max_children + 1 */
void LinearPickSeeds(std::vector<const Math::BoundingBox3D*> &boxes, 
					int& index_1, int& index_2)
{
    int min_max_x_i, max_min_x_i; 
	int min_max_y_i, max_min_y_i;
	int min_max_z_i, max_min_z_i;
	
    max_min_x_i = max_min_y_i = max_min_z_i = -1;
    min_max_x_i = min_max_y_i = min_max_z_i = -1;
	
    float min_max_x, max_min_x, min_max_y, max_min_y, min_max_z, max_min_z;
    float min_x, max_x, min_y, max_y, min_z, max_z;
	/* fixes un-init compile warning. These will get init */
	max_min_x = max_min_y = max_min_z = 0.0f;
	min_max_x = min_max_y = min_max_z = 0.0f;
	min_x = min_y = min_z = 0.0f;
	

	
    /* Find Minimum max value and Maximum min value for all elements 
	 * along each axis */
    for (int i=0; i < RTree::max_children + 1; ++i) {
        assert(boxes[i]);
        //boxes[i]->PrintBox();
       /* find max and min in each axis */
        /* X */
        if (i == 0 || boxes[i]->xmin() < min_x)
            min_x = boxes[i]->xmin();
        if (i == 0 || boxes[i]->xmax() > max_x)
            max_x = boxes[i]->xmax();
        /* Y */
        if (i == 0 || boxes[i]->ymin() < min_y)
            min_y = boxes[i]->ymin();
        if (i == 0 || boxes[i]->ymax() > max_y)
            max_y = boxes[i]->ymax();
        /* Z */
        if (i == 0 || boxes[i]->zmin() < min_z)
            min_z = boxes[i]->zmin();
        if (i == 0 || boxes[i]->zmax() > max_z)
            max_z = boxes[i]->zmax();

       /* Find Values that have a maximum min val or visa versa */
        /* X */
        if (i == 0 || boxes[i]->xmax() < min_max_x) {
            min_max_x_i = i;
            min_max_x = boxes[i]->xmax();
        }

        /*  Y */
        if (i == 0 || boxes[i]->ymax() < min_max_y) {
            min_max_y_i = i;
            min_max_y = boxes[i]->ymax();
        }
        /* Z */
        if (i == 0 || boxes[i]->zmax() < min_max_z) {
            min_max_z_i = i;
            min_max_z = boxes[i]->zmax();
        }

    } // END ungodly FOR

    for (int i=0; i < RTree::max_children + 1; ++i) {
        if (i != min_max_x_i && 
			(max_min_x_i == -1 || boxes[i]->xmin() > max_min_x)) {
            max_min_x_i = i;
            max_min_x = boxes[i]->xmin();
        }

        if (i != min_max_y_i &&
			(max_min_y_i == -1 || boxes[i]->ymin() > max_min_y)) {
            max_min_y_i = i;
            max_min_y = boxes[i]->ymin();
        }

        if (i != min_max_z_i && 
			(max_min_z_i == -1 || boxes[i]->zmin() > max_min_z)) {
            max_min_z_i = i;
            max_min_z = boxes[i]->zmin();
        }
    }

    /* Find Seperation and normalize */
    float x_sep = (max_min_x - min_max_x)  / std::abs(max_x - min_x);
    float y_sep = (max_min_y - min_max_y)  / std::abs(max_y - min_y);
    float z_sep = (max_min_z - min_max_z)  / std::abs(max_z - min_z);

    assert(x_sep> 0 || y_sep > 0 || z_sep > 0);

    /* X has max seperation */
    if (x_sep >= y_sep && x_sep >= z_sep) {
        index_1 = max_min_x_i;
        index_2 = min_max_x_i;
    }
    /* Y has max seperation */
    else if (y_sep >= z_sep) {
        index_1 = max_min_y_i;
        index_2 = min_max_y_i;

    }
    /* Else Z has max seperation */
    else {
        index_1 = max_min_z_i;
        index_2 = min_max_z_i;
    }

    assert(index_1 != index_2);
}

} /* namespace RTree_Detail */
