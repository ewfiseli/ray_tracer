/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "_rtree.h"
#include "_rtreenode.h"
#include "rtree/rtree.h"
#include "core/ray.h"
#include "core/intersection.h"

namespace RTree_Detail {
	
RTreeSearchVisitor::RTreeSearchVisitor(const Ray &ray) : ray_(ray)
{
	intersect_ = 0;
	min_dist = -10;
	tmp_intersect_ = nullptr;
	intersect_ = nullptr;
}

void RTreeSearchVisitor::Visit(const LeafNode *leaf)
{
	const Object *temp_obj;
	for (int i=0; i < leaf->Size(); ++i) {
		temp_obj = leaf->GetChild(i);
		tmp_intersect_ = temp_obj->Intersect(ray_);
		if (tmp_intersect_) {
			if (intersect_ && 
				(tmp_intersect_->dist_ < min_dist || min_dist < -1)) {
				delete intersect_;
				intersect_ = tmp_intersect_;
				min_dist = intersect_->dist_;
			}
			else if (!intersect_ && 
				(tmp_intersect_->dist_ < min_dist || min_dist < -1) ) {
				intersect_ = tmp_intersect_;
				min_dist = tmp_intersect_->dist_;
			} else {
				delete tmp_intersect_;
			}
		}
	}
}

void RTreeSearchVisitor::Visit(const TreeNode *node)
{
	for (int i=0; i < node->Size(); ++i) {
		if (this->min_dist > 0) {
			if (node->ChildBoundingBox(i)->Intersects(ray_, 0.0f, 
													this->min_dist + 0.01f))
				node->GetChild(i)->AcceptVisit(*this);
		} else {
			if (node->ChildBoundingBox(i)->Intersects(ray_, 0))
				node->GetChild(i)->AcceptVisit(*this);
		}
	}
}
	
} /* namespace RTree_Detail */