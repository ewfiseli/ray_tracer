/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTREE_RTREE_H
#define RTREE_RTREE_H

/* standard branch factors for RTREE, if they are not defined 
 * in the build */
#ifndef CONFIG_RTREE_MIN_BRANCH
#define RTREE_MIN_BRANCH 2
#else
#define RTREE_MIN_BRANCH CONFIG_RTREE_MIN_BRANCH
#endif 

#ifndef CONFIG_RTREE_MAX_BRANCH
#define RTREE_MAX_BRANCH 16
#else
#define RTREE_MAX_BRANCH CONFIG_RTREE_MAX_BRANCH
#endif


class Intersection;
class Object;
class Ray;
namespace RTree_Detail { class RTreeImpl; }


class RTree
{
public:
    RTree();
    ~RTree();

    /* Insert and object into the tree */
    void Insert(const Object *object);

    /*  Search for an object in a tree
    *   Return null if no object found
    */
    Intersection* Search(const Ray &ray) const;
    Intersection* Search(const Ray &ray, float max) const;
	
	static const int min_children = RTREE_MIN_BRANCH;
    static const int max_children = RTREE_MAX_BRANCH;
private:
	RTree_Detail::RTreeImpl *impl_;
};

#endif /* RTREE_RTREE_H */