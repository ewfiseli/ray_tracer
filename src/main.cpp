/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <time.h>

#include "core/scene.h"
#include "core/camera.h"
#include "core/image.h"
#include "core/pixelraythread.h"
#include "core/rgbimage.h"
#include "core/inputfilereader.h"

#define CONFIG_MP


#ifdef CONFIG_MP
#   include <thread>
#endif


void usage(void)
{
	std::cout << "Usage: rtracer [-j [#threads]] filename" << std::endl;
}

/* print the build info */
void build_info(void)
{
	std::cout << "Ray Tracer Build Info:" << std::endl;

#if defined(CONFIG_REC_DEPTH)
	std::cout << "build: CONFIG_REC_DEPTH=" << CONFIG_REC_DEPTH << std::endl;
#endif 
	
#if defined(CONFIG_USE_RTREE)
	std::cout << "build: CONFIG_USE_RTREE" << std::endl;
#	if defined(CONFIG_RTREE_MAX_BRANCH)
	std::cout << "build: CONFIG_RTREE_MAX_BRANCH=" << CONFIG_RTREE_MAX_BRANCH
			  << std::endl;
#	endif		  
#	if defined(CONFIG_RTREE_MIN_BRANCH)
	std::cout << "build: CONFIG_RTREE_MIN_BRANCH=" << CONFIG_RTREE_MIN_BRANCH
			  << std::endl;
#	endif 		  
#endif /* defined (CONFIG_USE_RTREE) */
			  
#if defined(CONFIG_SPHERE_GEOM_INTERSECT)
	std::cout << "build: CONFIG_SPHERE_GEOM_INTERSECT" << std::endl;
#endif
	
#if defined(CONFIG_TRACE_5)
	std::cout << "build: CONFIG_TRACE_5" << std::endl;
#endif
	
#if defined(CONFIG_SHADOW_5)
	std::cout << "build: CONFIG_SHADOW_5" << std::endl;
#endif
}

int main(int argc, char *argv[])
{
    long start_time = time(NULL);
 
	if (argc < 2 || argc > 4) {
		usage();
		return 1;
	}
	
	std::string filename;
	int num_threads;
	if(argc == 2) {
		/* must only be filename */
		filename = argv[1];
		num_threads = 1;
	} else if (argc == 3) {
		if (strcmp(argv[1], "-j") != 0)  {
			std::cout << "Invalid option: " << argv[1] << std::endl;
			return 1;
		}
		/* -j set, filename is 3rd arg */
		num_threads = std::thread::hardware_concurrency();
		if (num_threads <= 0) {
			std::cout << "FATAL ERROR: std::thread::hardware_concurrency <= 0";
			std::cout << std::endl;
			return 1;
		}
		filename = std::string(argv[2]);
	} else {
		/* argc == 4 */
		if (strcmp(argv[1], "-j") != 0)  {
			std::cout << "Invalid option: " << argv[1] << std::endl;
			return 1;
		}
		std::string num_t_str(argv[2]);
		std::istringstream istr(num_t_str);
		istr >> num_threads;
		filename = std::string(argv[3]);
	}
	
/* if we are debugging, print build options on run */
#ifndef NDEBUG
	build_info();
#endif
	
    std::cout << "Reading Scene and Camera Information From File: ";
	std::cout << filename << std::endl;
	if (num_threads > 1) 
		std::cout << "Using " << num_threads << " threads" << std::endl;
		
    Scene* scene = nullptr;
    Camera* camera = nullptr;
    int img_rows, img_cols;

    InputFileReader file_reader;
    FileOutput_t* output = file_reader.ParseFile(filename.c_str());
    scene = output->scene;
	output->scene = nullptr;
	
    camera = output->camera;
	output->camera = nullptr;
	
    img_rows = output->height;
    img_cols = output->width;
	
	delete output;

    assert (scene && camera && img_rows != 0 && img_cols != 0);
    Math::Copy(scene->camera_pos_, camera->eye_);

    //create image
    Image* image = new Image(img_rows, img_cols);
	

    std::vector<PixelRayThread*> ray_threads(num_threads);
    for (int i=0; i < num_threads; ++i) {
        ray_threads[i] = new PixelRayThread(scene, camera, image);
    }

    long thread_num = 0;
    for (int i = 0; i < img_rows; ++i) {
        for (int j = 0; j < img_cols; ++j) {
            ray_threads[thread_num % num_threads]->Insert(i, j);
            ++thread_num;
        }
    }

    std::cout << "Starting Tracing" << std::endl;
	
	std::vector<std::thread*> threads(num_threads);
    for(int i=0; i < num_threads; ++i) {
		threads[i] = new std::thread(&PixelRayThread::run, ray_threads[i]);
    }

    for (int i=0; i < num_threads; ++i) {
		threads[i]->join();
		delete threads[i];
		delete ray_threads[i];
	}

    image->SaveImage(filename.c_str());

    delete camera;
    delete scene;
    delete image;

    std::cout << "All Done! Image Saved" << std::endl;
	std::cout << "Time: " << time(NULL) - start_time << std::endl;
	return 0;
}
