/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "objects/primatives.h"
#include "core/ray.h"
#include "core/intersection.h"
#include "core/material.h"

Quad::Quad(const Math::vect_t &p1, const Math::vect_t &p2,
		   const Math::vect_t &p3, const Math::vect_t &p4,
           const Material &mat) : Object(mat)
{
    part1_ = Triangle(p1, p2, p3, mat);
    part2_ = Triangle(p3, p4, p1, mat);
    GenerateBoundingBox();
}

void Quad::GenerateBoundingBox()
{
    bounding_box_ = *part1_.BoundingBox();
    bounding_box_.Enlarge(*part2_.BoundingBox());
}

const Math::BoundingBox3D* Quad::BoundingBox() const
{
    return &bounding_box_;
}

Intersection* Quad::Intersect(const Ray& ray) const
{
    Intersection* temp_intersect;
    temp_intersect = part1_.Intersect(ray);

    if (temp_intersect)
        return temp_intersect;

   return part2_.Intersect(ray);
}