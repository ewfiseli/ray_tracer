/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef OBJECTS_OBJECT_H
#define OBJECTS_OBJECT_H

#include "core/material.h"

class Ray;
class Intersection;

namespace Math {
class BoundingBox3D;
}

class Object
{
public:
    Object () {}
    Object(const Material &material) {material_ =  material;}
    virtual ~Object() { }

    /* To Be implemented by children */
    virtual Intersection*  Intersect(const Ray &ray) const = 0;
    virtual const Math::BoundingBox3D *BoundingBox() const = 0;

    const Material* GetMaterial() const {return &material_;}
private:
    Material material_;
};

#endif /* OBJECTS_OBJECT_H */