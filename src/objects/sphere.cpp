/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "objects/primatives.h"
#include "core/ray.h"
#include "core/intersection.h"
#include "core/material.h"
#include "math/mathdefs.h"

#include <cmath>

Sphere::Sphere(const Math::vect_t &center, float radius, const Material &material) :
    Object(material)
{
    Math::Copy(center_, center);
    radius_ = radius;
    radius_square_ = radius_*radius_;
    GenerateBoundingBox();
}

void Sphere::GenerateBoundingBox()
{
    bounding_box_.xmax(center_[0] + radius_);
    bounding_box_.xmin(center_[0] - radius_);

    bounding_box_.ymax(center_[1] + radius_);
    bounding_box_.ymin(center_[1] - radius_);

    bounding_box_.zmax(center_[2] + radius_);
    bounding_box_.zmin(center_[2] - radius_);
    bounding_box_.CalculateVolume();
}

const Math::BoundingBox3D* Sphere::BoundingBox() const
{
    return  &bounding_box_;
}

Intersection* Sphere::Intersect(const Ray &ray) const
{

#if defined(CONFIG_SPHERE_GEOM_INTERSECT)
    //GEOMETRIC METHOD
    //vector from base to center of sphere
    Math::vect_t OS;
    Math::SubtractVector(OS, center_, ray.base_);

    float Tp = Math::DotProduct(OS, ray.direction_);

    if (Tp < 0.0f)
        return nullptr;

    float e = Math::DotProduct(OS, OS) - (Tp*Tp);
    float t = radius_square_ - e;

    if (t < 0.0f)
        return nullptr;

    t = sqrt(t);
    t = Tp - t;

    float t2 = Tp + t;

    t = std::min(t, t2);

    if (fabs(t) < EPSILON)
        return nullptr;
#else
    //ALGEBRAIC METHOD
    // Get vector from center to the base of the ray
    Math::vect_t OP;
    Math::SubtractVector(OP, ray.base_, center_);

    float b = 2.0f * Math::DotProduct(OP, ray.direction_);
    float c = Math::DotProduct(OP, OP) - radius_square_;
    float d = b*b - 4*c;

    if (d < 0)
        return nullptr;

    d = sqrt(d);
    float t1 = (-b + d)/2.0f;
    float t2 = (-b - d)/2.0f;

    if (t1 > t2) {
        float temp = t1;
        t1 = t2;
        t2 = temp;
    }

    float t;
    if (t1 < EPSILON && t2 <= EPSILON)
        return nullptr;
    if (t1 < EPSILON)
        t = t2;
    else
        t = t1;

#endif
    Intersection* intersect = new Intersection();
    intersect->mat_ = this->GetMaterial();

    // Get Intersection Point
    Math::ExtendVector(intersect->intersect_point_, ray.base_, ray.direction_, t);

    //Get Normal at point
    Math::SubtractVector(intersect->normal_, intersect->intersect_point_, center_);
    Math::Normalize(intersect->normal_);

    //Recalculate Intersection point using normal
    Math::ExtendVector(intersect->intersect_point_, center_, 
					   intersect->normal_, (radius_ + EPSILON));

    //Get distance from base to intersection
    intersect->dist_ = Math::DistBetween(intersect->intersect_point_, ray.base_);
    assert(intersect->dist_ > -0.1 && ! (intersect->dist_ != intersect->dist_));

    return intersect;
}
