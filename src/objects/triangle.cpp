/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "objects/primatives.h"
#include "core/ray.h"
#include "core/intersection.h"
#include "core/material.h"
#include "math/mathdefs.h"

#include <algorithm>

Triangle::Triangle(const Math::vect_t &p1, const Math::vect_t &p2,
				   const Math::vect_t &p3, const Material &material) 
				: Object(material)
{
    //Copy Triangle Points
    Math::Copy(p1_, p1);
    Math::Copy(p2_, p2);
    Math::Copy(p3_, p3);

    //initalizie edges E1_ and E2_
    Math::SubtractVector(E1_, p2_, p1_);
    Math::SubtractVector(E2_, p3_, p1_);

    CalculateBoundingBox();
    CalculateNormal();
}

void Triangle::CalculateBoundingBox()
{
    bounding_box_.xmax( std::max(p1_[0], std::max(p2_[0], p3_[0])) );
    bounding_box_.xmin( std::min(p1_[0], std::min(p2_[0], p3_[0])) );

    bounding_box_.ymax( std::max(p1_[1], std::max(p2_[1], p3_[1])) );
    bounding_box_.ymin( std::min(p1_[1], std::min(p2_[1], p3_[1])) );

    bounding_box_.zmax( std::max(p1_[2], std::max(p2_[2], p3_[2])) );
    bounding_box_.zmin( std::min(p1_[2], std::min(p2_[2], p3_[2])) );

    bounding_box_.CalculateVolume();
}

const Math::BoundingBox3D* Triangle::BoundingBox() const
{
    return  &bounding_box_;
}

void Triangle::CalculateNormal()
{
    Math::CrossProduct(normal_, E2_, E1_);
    Math::Normalize(normal_);
}

/* Code Taken (and modified) From given code */
Intersection* Triangle::Intersect(const Ray &ray) const
{
    Math::vect_t Pvect;
    Math::CrossProduct(Pvect, ray.direction_, E2_);
    float det = Math::DotProduct(E1_, Pvect);
    assert(det == det);

    if ((det > -EPSILON && det < EPSILON))
        return nullptr;


    float inv_det = 1.0 / det;
    assert(inv_det == inv_det);

    Math::vect_t Tvect;
    Math::SubtractVector(Tvect, ray.base_, p1_);

    float beta = Math::DotProduct(Tvect, Pvect) * inv_det;
    assert(beta == beta);

    if (beta < 0.0 || beta > 1.0)
        return 0;

    Math::vect_t Qvect;
    Math::CrossProduct(Qvect, Tvect, E1_);

    float gamma = Math::DotProduct(ray.direction_, Qvect) * inv_det;
    assert(gamma == gamma);
    if (gamma < 0.0 || beta + gamma > 1.0)
        return 0;

    float T = Math::DotProduct(E2_, Qvect) * inv_det;


    if (T < 0)
        return 0;

    Intersection *intersect = new Intersection();
    intersect->mat_ = (this->GetMaterial());

    //Get Intersection point
    Math::ExtendVector(intersect->intersect_point_, ray.base_, ray.direction_, T);

    //get normal
    Math::Copy(intersect->normal_, normal_);

    //scale intersection along normal
    Math::vect_t temp_norm;
    Math::Copy(temp_norm, normal_);
    Math::MultiplyVal(temp_norm,  EPSILON);
    Math::AddVector(intersect->intersect_point_, temp_norm);

    intersect->dist_ = Math::DistBetween(ray.base_, intersect->intersect_point_);

    return intersect;
}