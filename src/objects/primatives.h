/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef OBJECTS_PRIMATIVES_H
#define OBJECTS_PRIMATIVES_H

#include "objects/object.h"
#include "math/vector3d.h"
#include "math/bounds3d.h"

class Ray;
class Intersection;
class Material;

class Triangle : public Object
{
public:
    Triangle () {}
    Triangle(const Math::vect_t &p1, const Math::vect_t &p2, 
			 const Math::vect_t &p3, const Material& material);

    /* Implementation for Virtual Object Methods */
    Intersection* Intersect(const Ray &ray) const;
    const Math::BoundingBox3D* BoundingBox() const;
private:
    void CalculateNormal();
    void CalculateBoundingBox();
    Math::vect_t p1_, p2_, p3_;
    Math::vect_t E1_, E2_;
    Math::vect_t normal_;
    Math::BoundingBox3D bounding_box_;
};


class Quad : public Object
{
public:
    Quad(const Math::vect_t& p1, const Math::vect_t &p2, 
		 const Math::vect_t& p3, const Math::vect_t &p4, const Material &mat);
	
    Intersection* Intersect(const Ray &ray) const;
    const Math::BoundingBox3D* BoundingBox() const;
private:
    void GenerateBoundingBox();
    Triangle part1_, part2_;
    Math::BoundingBox3D bounding_box_;
};


class Sphere : public Object
{
public:
    Sphere(const Math::vect_t &center, float radius, const Material &material);
    ~Sphere() {}
    Intersection* Intersect(const Ray &ray) const;
    const Math::BoundingBox3D* BoundingBox() const;
private:
    void GenerateBoundingBox();
    Math::vect_t center_;
    float radius_;
    float radius_square_;
    Math::BoundingBox3D bounding_box_;
};

#endif /* OBJECTS_PRIMATIVES_H */