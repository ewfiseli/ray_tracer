/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MATH_BOUNDS3D_H
#define MATH_BOUNDS3D_H

#include "math/vector3d.h"
#include "core/ray.h"

#include <algorithm>
#include <cstdio>
#include <cmath>


namespace Math {

typedef float Bounds3D_t[2][3];

inline void CopyBounds3D(Bounds3D_t &dest, const Bounds3D_t &bounds)
{
    dest[0][0] = bounds[0][0];
    dest[0][1] = bounds[0][1];
    dest[0][2] = bounds[0][2];

    dest[1][0] = bounds[1][0];
    dest[1][1] = bounds[1][1];
    dest[1][2] = bounds[1][2];
}

class BoundingBox3D
{
public:
    BoundingBox3D () {
		_Reset();
        volume_ = GetVolume(bounds_);
    }

    BoundingBox3D(float x_min, float x_max, float y_min,
                  float y_max, float z_min, float z_max) {

        bounds_[0][0] = x_min;
        bounds_[0][1] = y_min;
        bounds_[0][2] = z_min;

        bounds_[1][0] = x_max;
        bounds_[1][1] = y_max;
        bounds_[1][2] = z_max;
        volume_ = GetVolume(bounds_);
    }

    BoundingBox3D& operator= (const BoundingBox3D& box) {
        Math::CopyBounds3D(bounds_, box.bounds_);
        volume_ = box.volume_;
        return (*this);
    }
    
    inline void CopyBounds(const BoundingBox3D &box) {
		Math::CopyBounds3D(bounds_, box.bounds_);
		volume_ = box.volume_;
	}
    
    inline void _Reset() {
        bounds_[0][0] = 0;
        bounds_[0][1] = 0;
        bounds_[0][2] = 0;

        bounds_[1][0] = 0;
        bounds_[1][1] = 0;
        bounds_[1][2] = 0;
	}

    float Volume() const { return volume_; }

    void CalculateVolume() {volume_ = GetVolume(bounds_);}

    float RequiredEnlargement(const BoundingBox3D& other_bounds) const
    {
        Bounds3D_t temp;
        temp[0][0] = std::min(xmin(), other_bounds.xmin());
        temp[1][0] = std::max(xmax(), other_bounds.xmax());

        temp[0][1] = std::min(ymin(), other_bounds.ymin());
        temp[1][1] = std::max(ymax(), other_bounds.ymax());

        temp[0][2] = std::min(zmin(), other_bounds.zmin());
        temp[1][2] = std::max(zmax(), other_bounds.zmax());

        return (GetVolume(temp) - volume_);
    }

    /* Enlarge the box and update the volume so that it includes
     * The other box
     */
    void Enlarge(const BoundingBox3D &other_bounds) {

        this->bounds_[0][0] = std::min(this->xmin(), other_bounds.xmin());
        this->bounds_[1][0] = std::max(this->xmax(), other_bounds.xmax());

        this->bounds_[0][1] = std::min(this->ymin(), other_bounds.ymin());
        this->bounds_[1][1] = std::max(this->ymax(), other_bounds.ymax());

        this->bounds_[0][2] = std::min(this->zmin(), other_bounds.zmin());
        this->bounds_[1][2] = std::max(this->zmax(), other_bounds.zmax());

        this->volume_ = GetVolume(this->bounds_);
    }

    float GetVolume(Bounds3D_t& bounds) const
    {
        return ((bounds[1][0] - bounds[0][0]) * (bounds[1][1] - bounds[0][1]) * (bounds[1][2] - bounds[0][2]));
    }

    /* Getters for values */
    float xmin() const
        {return bounds_[0][0];}
    float xmax() const
        {return bounds_[1][0];}
    float ymin() const
        {return bounds_[0][1];}
    float ymax() const
        {return bounds_[1][1];}
    float zmin() const
        {return bounds_[0][2];}
    float zmax() const
        {return bounds_[1][2];}

    void xmin(float x)
        {bounds_[0][0] = x;}
    void xmax(float x)
        {bounds_[1][0] = x;}
    void ymin(float y)
        {bounds_[0][1] = y;}
    void ymax(float y)
        {bounds_[1][1] = y;}
    void zmin(float z)
        {bounds_[0][2] = z;}
    void zmax(float z)
        {bounds_[1][2] = z;}

    /*
    * from: http://people.csail.mit.edu/amy/papers/box-jgt.pdf
    */
    bool Intersects(const Ray& ray, float  min_t, 
					float  max_t = 10000000.0f) const
    {
		UNUSED(min_t);

        float  tmin, tmax, tymin, tymax, tzmin, tzmax;

        tmin = ((bounds_[ ray.sign_[0]    ][0] - 
				ray.base_[0]) * ray.inv_direction_[0]);
		
        tmax = ((bounds_[1 - ray.sign_[0] ][0] - 
				ray.base_[0]) * ray.inv_direction_[0]);

        tymin = ((bounds_[ ray.sign_[1]    ][1] - 
				ray.base_[1]) * ray.inv_direction_[1]);
		
        tymax = ((bounds_[1 - ray.sign_[1] ][1] - 
				ray.base_[1]) * ray.inv_direction_[1]);

        if ((tmin > tymax) || (tymin > tmax) )
            return false;
        if (tymin > tmin)
            tmin = tymin;
        if (tymax < tmax)
            tmax = tymax;

        tzmin = ((bounds_[ ray.sign_[2]    ][2] - 
				ray.base_[2]) * ray.inv_direction_[2]);
		
        tzmax = ((bounds_[1 - ray.sign_[2] ][2] - 
				ray.base_[2]) * ray.inv_direction_[2]);

        if ((tmin > tzmax) || (tzmin > tmax))
            return false;
        if (tzmin > tmin)
            tmin = tzmin;
        if (tzmax < tmax)
            tmax = tzmax;


        return true;
        return((tmin < max_t) && tmax > 0.0);

    }

    void PrintBox()
    {
        std::printf("X: %.2f %.2f   Y: %.2f %.2f   Z: %.2f %.2f \n"
               , xmin(), xmax(), ymin(), ymax(), zmin(), zmax()) ;
    }

    float volume_;
    Bounds3D_t bounds_;
};

} /* namespace Math */
#endif /* MATH_BOUNDS3D_H */
