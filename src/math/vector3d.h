/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MATH_VECTOR3D_H
#define MATH_VECTOR3D_H

#include <cmath>
#include <cassert>

#define INIT_VECTOR(x) x[0] = 0.0f; \
					  x[1] = 0.0f; \
					  x[2] = 0.0f

namespace Math {

typedef float vect_t[3];

inline void AddVector(vect_t &dest, const vect_t &v1, const vect_t& v2)
{
    dest[0] = v1[0] + v2[0];
    dest[1] = v1[1] + v2[1];
    dest[2] = v1[2] + v2[2];
}

inline void AddVector(vect_t& v1, const vect_t &v2) 
{
	AddVector(v1, v1, v2);
}

inline void SubtractVector(vect_t  &dest, const vect_t &v1, const vect_t &v2)
{
    dest[0] = v1[0] - v2[0];
    dest[1] = v1[1] - v2[1];
    dest[2] = v1[2] - v2[2];
}

inline void SubtractVector(vect_t &v1, const vect_t &v2)
{
	SubtractVector(v1, v1, v2);
}


inline float DotProduct(const vect_t &v1, const vect_t &v2)
{
    float val (v1[0] * v2[0]  +  v1[1] * v2[1]  +  v1[2] * v2[2]);

    assert(val == val);
    return val;
}

inline void CrossProduct(vect_t &dest, const vect_t& v1, const vect_t &v2)
{
             dest[0]=v1[1]*v2[2] - v1[2]*v2[1];
             dest[1]=v1[2]*v2[0] - v1[0]*v2[2];
             dest[2]=v1[0]*v2[1] - v1[1]*v2[0];
}

inline float DistBetween(const vect_t &v1, const vect_t &v2)
{

   float val = sqrt((v2[0]-v1[0])*(v2[0]-v1[0]) +
					(v2[1]-v1[1])*(v2[1]-v1[1]) +
					(v2[2]-v1[2])*(v2[2]-v1[2]) );
   assert(val == val);
   return val;
 }

inline float Length(const vect_t &v)
{
    float val = sqrt(DotProduct(v, v));
    assert(val == val);
    return val;
}

inline void Normalize(vect_t& v)
{
    float len = Length(v);
    v[0] /= len;
    v[1] /= len;
    v[2] /= len;
}

inline void ExtendVector(vect_t &dest, const vect_t& base, const vect_t& direction, float t)
{
    dest[0] = base[0] + (direction[0] * t);
    dest[1] = base[1] + (direction[1] * t);
    dest[2] = base[2] + (direction[2] * t);
}

inline void Copy(vect_t& dest, const vect_t &vect)
{
    dest[0] = vect[0];
    dest[1] = vect[1];
    dest[2] = vect[2];
}

inline void MultiplyVal(vect_t& vect, float val)
{
    vect[0] *= val;
    vect[1] *= val;
    vect[2] *= val;
}

inline void MultiplyVal(vect_t& dest, const vect_t& vect, float val)
{
    dest[0] = vect[0] * val;
    dest[1] = vect[1] * val;
    dest[2] = vect[2] * val;
}

inline void Reverse(vect_t& dest, const vect_t& vect)
{
    dest[0] = -vect[0];
    dest[1] = -vect[1];
    dest[2] = -vect[2];

    assert (dest[0] == dest[0]);
    assert (dest[1] == dest[1]);
    assert (dest[2] == dest[2]);
}

inline void Reverse(vect_t& vect) 
{
	Reverse(vect, vect);
}

inline void GetOrthogVector(vect_t& dest, const vect_t& vect)
{

    dest[0] = vect[2];
    dest[1] = vect[1];
    dest[2] = -vect[0];

}

inline void NANCheck(vect_t& vect)
{
    if (vect[0] != vect[0])
        vect[0] = 0;
    if (vect[1] != vect[1])
        vect[1] = 0;
    if (vect[2] != vect[2])
        vect[2] = 0;
}

} /* namespace Math */
#endif /* MATH_VECTOR3D_H */