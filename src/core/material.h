/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_MATERIAL_H
#define CORE_MATERIAL_H

#include "core/color.h"

class Material
{
public:
	Material() { }
	
    Material(const Material& mat) {
        ambient_r_ = mat.ambient_r_;
        diffuse_r_ = mat.diffuse_r_;
        specular_r_ = mat.specular_r_;
        reflected_r_ = mat.reflected_r_;
        refracted_r_ = mat.refracted_r_;
        shine_ = mat.shine_;
    }

    Material& operator= (const Material& mat) {
        ambient_r_ = mat.ambient_r_;
        diffuse_r_ = mat.diffuse_r_;
        specular_r_ = mat.specular_r_;
        reflected_r_ = mat.reflected_r_;
        refracted_r_ = mat.refracted_r_;

        shine_ = mat.shine_;

        return (*this);
    }

	Material(const Color& ambient_r, const Color& diffuse_r, 
				   const Color& specular_r, float shine, float refract) {
		ambient_r_ = ambient_r;
		diffuse_r_ = diffuse_r;
		specular_r_ = specular_r;
		shine_ = shine;
		refract_ = refract;
	}

    Color diffuse_r_, specular_r_, ambient_r_, reflected_r_, refracted_r_;
    float shine_, refract_;
};

#endif //MATERIAL_H
