/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_INPUTFILEREADER_H
#define CORE_INPUTFILEREADER_H

#include "math/vector3d.h"

#include <fstream>
#include <string>
#include <iostream>
#include <cstdlib>

/* Object classes */
class Object;
class Sphere;
class Triangle;
class Quad;
/* Core classes */
class Light;
class Scene;
class Camera;
class Material;
class Color;

typedef struct file_output_struct {
    Scene* scene;
    Camera* camera;
    int width, height;
} FileOutput_t;

class InputFileReader
{
public:
	InputFileReader() { }
    FileOutput_t* ParseFile(const char* file_name) const;
private:
    void ParseImageSpecs(std::ifstream &file, int &width, int &height) const;
    Camera* ParseCamera(std::ifstream &file) const;

    Sphere* ParseSphere(std::ifstream &file) const;
    Triangle* ParseTriangle(std::ifstream &file) const;
    Quad* ParseQuad(std::ifstream &file) const;

    Light* ParseLight(std::ifstream &file) const;

    void ParsePoint(std::ifstream &file, Math::vect_t& vect) const;
    void ParseVector(std::ifstream &file, Math::vect_t& vect) const;
    Material* ParseMaterial(std::ifstream &file) const;
    Color* ParseColor(std::ifstream &file) const;

    bool ReadAssignment(std::ifstream &file) const;
    bool ReadOpenBlock(std::ifstream &file) const;
    bool ReadToken(std::ifstream &file, char* str_buff) const;
    bool ReadInteger(std::ifstream &file, int &x) const;
    bool ReadFloat(std::ifstream &file, float &x) const;

	template <typename Str1T, typename Str2T, typename Str3T>
    void Error(const Str1T &error_str, const Str2T &from_str, 
			   const Str3T &detail_str) const
	{
			std::cout << "Fatal Error " << error_str << from_str << " in ";
			std::cout << detail_str << std::endl;
			std::exit(1);
	}
	
	template<typename Str1T, typename Str2T, typename Str3T>
    void Warning(const Str1T &warning_str, const Str2T &from_str, 
				 const Str3T &detail_str) const
	{
		std::cout << warning_str << detail_str << " in " 
				  << from_str << std::endl;
	}

    bool CheckFile(std::ifstream &file) const;

    static const std::string KFileReadError; // = "Error Reading File: ";
    static const std::string KSyntaxError;// = "Invalid Syntax: ";
    static const std::string KIncompleteTypeError;// = "Incomplete Description in File: ";

    static const std::string KRedefinedWarning;// = "Warning Field Redefined: ";
    static const std::string KUndefinedWarning; // "Warning Field Undefined: ";

    /* Syntax Strings */
    static const std::string KAssignmentString; // = "=";
    static const std::string KBlockOpenString;//  = "{";
    static const std::string KBlockCloseString; // = "}";
    static const std::string KEndString; // = "END";

    /* Main Class Type Strings */
    static const std::string KLightTypeString; // = "Light";
    static const std::string KAmbientSceneLightTypeString; // = "Ambient_Scene_Light";
    static const std::string KBackgroundSceneLightTypeString; //= "Background_Scene_Light";
    static const std::string KImageSpecTypeString; // = "Image_Spec";
    static const std::string KCameraTypeString; // = "Camera";
    static const std::string KMaterialTypeString; // = "Material";
    static const std::string KPointTypeString; // "Point3D";
    static const std::string KColorTypeString; //  = "Color";
    static const std::string KVectorTypeString; // = "Vector3D";
    /*Object Type Strings */
    static const std::string KSphereTypeString; // = "Sphere";
    static const std::string KTriangleTypeString; // = "Triangle";
    static const std::string KQuadTypeString; // = "Quad";
    /* Field Type Strings */
    static const std::string KWidthTypeString; // = "Width";
    static const std::string KHeightTypeString; // = "Height";
    /* Camera Fields */
    static const std::string KFOVTypeString; // = "FOV"
    static const std::string KEyePositionTypeString; // = "Eye_Position
    static const std::string KUpVectorTypeString; // = "Up_Vector"
    static const std::string KLookVectorTypeString; // = "Look_Vector"
    /* Sphere Fields */
    static const std::string KRadiusTypeString; //  = "Radius";
    static const std::string KCenterPointTypeString; // = "Center_Point;
    /* Triangle and Quad Fields */
    static const std::string KPointOneTypeString; // = "Point_One";
    static const std::string KPointTwoTypeString; // = Point_Two";
    static const std::string KPointThreeTypeString; // = "Point_Three";
    static const std::string KPointFourTypeString; // = "Point_Four";
    /* Light and Material Fields */
    static const std::string KAmbientColorTypeString; // = "Ambient_Color";
    static const std::string KDiffuseColorTypeString; // = "Diffuse_Color";
    static const std::string KSpecularColorTypeString; // = "Specular_Color";
    /* Remaining Material Values */
    static const std::string KReflectedColorTypeString; // = "Reflected_Color";
    static const std::string KRefractedColorTypeString; // = "Refracted_Color";
    static const std::string KShineTypeString; // = "Shine";
	
private:
	DISALLOW_COPY_AND_ASSIGN(InputFileReader);
};

#endif /* CORE_INPUTFILEREADER_H */
