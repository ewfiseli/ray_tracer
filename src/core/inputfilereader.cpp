/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "core/inputfilereader.h"
#include "core/camera.h"
#include "core/scene.h"
#include "core/light.h"
#include "core/material.h"
#include "math/vector3d.h"
#include "objects/object.h"
#include "objects/primatives.h"

#include <cassert>

const std::string InputFileReader::KFileReadError = "Error Reading File: ";
const std::string InputFileReader::KSyntaxError = "Invalid Syntax: ";
const std::string InputFileReader::KIncompleteTypeError = "Incomplete Description in File: ";

const std::string InputFileReader::KRedefinedWarning = "Warning Field Redefined: ";
const std::string InputFileReader::KUndefinedWarning = "Warning Field Undefined";

/* Syntax Strings */
const std::string InputFileReader::KAssignmentString = "=";
const std::string InputFileReader::KBlockOpenString = "{";
const std::string InputFileReader::KBlockCloseString = "}";
const std::string InputFileReader::KEndString = "END";

/* Main Class Type Strings */
const std::string InputFileReader::KLightTypeString = "Light";
const std::string InputFileReader::KImageSpecTypeString = "Image_Spec";
const std::string InputFileReader::KCameraTypeString = "Camera";
const std::string InputFileReader::KMaterialTypeString = "Material";
const std::string InputFileReader::KAmbientSceneLightTypeString = "Ambient_Scene_Light";
const std::string InputFileReader::KBackgroundSceneLightTypeString = "Background_Scene_Light";
const std::string InputFileReader::KPointTypeString = "Point3D";
const std::string InputFileReader::KColorTypeString  = "Color";
const std::string InputFileReader::KVectorTypeString = "Vector3D";
/*Object Type Strings */
const std::string InputFileReader::KSphereTypeString = "Sphere";
const std::string InputFileReader::KTriangleTypeString = "Triangle";
const std::string InputFileReader::KQuadTypeString = "Quad";
/* Field Type Strings */
const std::string InputFileReader::KWidthTypeString = "Width";
const std::string InputFileReader::KHeightTypeString = "Height";
/* Camera Fields */
const std::string InputFileReader::KFOVTypeString = "FOV";
const std::string InputFileReader::KEyePositionTypeString = "Eye_Position";
const std::string InputFileReader::KUpVectorTypeString = "Up_Vector";
const std::string InputFileReader::KLookVectorTypeString = "Look_Vector";
/* Sphere Fields */
const std::string InputFileReader::KRadiusTypeString = "Radius";
const std::string InputFileReader::KCenterPointTypeString = "Center_Point";
/* Triangle and Quad Fields */
const std::string InputFileReader::KPointOneTypeString = "Point_One";
const std::string InputFileReader::KPointTwoTypeString = "Point_Two";
const std::string InputFileReader::KPointThreeTypeString = "Point_Three";
const std::string InputFileReader::KPointFourTypeString = "Point_Four";
/* Light and Material Fields */
const std::string InputFileReader::KAmbientColorTypeString = "Ambient_Color";
const std::string InputFileReader::KDiffuseColorTypeString = "Diffuse_Color";
const std::string InputFileReader::KSpecularColorTypeString = "Specular_Color";
/* Remaining Material Values */
const std::string InputFileReader::KReflectedColorTypeString = "Reflected_Color";
const std::string InputFileReader::KRefractedColorTypeString = "Refracted_Color";
const std::string InputFileReader::KShineTypeString = "Shine";



FileOutput_t* InputFileReader::ParseFile(const char* file_name) const
{
    std::ifstream in_file;
    in_file.open(file_name);
    CheckFile(in_file);
    char str_buff[100];

    FileOutput_t *file_output = new FileOutput_t;
    file_output->scene = new Scene();

    bool read_image_spec = false;
    bool read_camera_spec = false;
    bool read_ambient_light_spec = false;
    bool read_background_light = false;

    /* Parse File while not exit */
    while (true) {
        ReadToken(in_file, str_buff);
        if (KImageSpecTypeString == str_buff) {
            if (read_image_spec)
                Warning(KRedefinedWarning, "ParseFile", KImageSpecTypeString);

            ParseImageSpecs(in_file, file_output->width, file_output->height);
            read_image_spec = true;
        }
        else if (KBackgroundSceneLightTypeString == str_buff) {
            if (read_background_light)
                Warning(KRedefinedWarning, "ParseFile", 
						KBackgroundSceneLightTypeString);

            Color* temp_col = ParseColor(in_file);
            assert(temp_col);
            file_output->scene->backgroud_light_ = *temp_col;
            delete temp_col;
        }
        else if (KLightTypeString == str_buff) {
            Light* light = ParseLight(in_file);
            assert(light);
            file_output->scene->AddLight(light);
        }
        else if (KCameraTypeString == str_buff) {
            Camera* cam = ParseCamera(in_file);
            assert(cam);
            file_output->camera = cam;
            read_camera_spec = true;
        }
        /* Read Object Statements */
        else if (KSphereTypeString == str_buff) {
            Sphere* sphere = ParseSphere(in_file);
            assert(sphere);
            file_output->scene->AddObject(sphere);
        }
        else if (KTriangleTypeString == str_buff) {
            Triangle* triangle = ParseTriangle(in_file);
            assert(triangle);
            file_output->scene->AddObject(triangle);
        }
        else if (KQuadTypeString == str_buff) {
            Quad* quad = ParseQuad(in_file);
            assert(quad);
            file_output->scene->AddObject(quad);
        }
        /* Read Light */
        else if (KLightTypeString == str_buff) {
            Light* light = ParseLight(in_file);
            assert(light);
            file_output->scene->AddLight(light);
        }
        else if (KAmbientSceneLightTypeString == str_buff) {
            if (read_ambient_light_spec)
                Warning(KRedefinedWarning, "ParseFile", 
						KAmbientSceneLightTypeString);
				
            Color* light = ParseColor(in_file);
            assert(light);
            file_output->scene->ambient_light_ = *light;
            read_ambient_light_spec = true;
            delete light;
        }
        else if (KEndString == str_buff) {
            if (read_image_spec && read_camera_spec) {

                if (read_ambient_light_spec == false)
                    Warning(KUndefinedWarning, "ParseFile", 
							KAmbientSceneLightTypeString);

                return file_output;
            } else {
                Error(KIncompleteTypeError, "ParseFile", 
					  "Missing Camera or Image spec");
            }
        } else {
            Error(KSyntaxError, "ParseFile", str_buff);
        }
    }

}


void InputFileReader::ParseImageSpecs(std::ifstream &file, 
									int &width, int &height) const
{
    char str_buff[100];
    ReadOpenBlock(file);

    bool read_width = false;
    bool read_height = false;
    while (true) {
        ReadToken(file, str_buff);

        if (KWidthTypeString == str_buff) {
            if (read_width)
                Warning(KRedefinedWarning, "ParseImageSpecs", KWidthTypeString);

            ReadAssignment(file);
            ReadInteger(file, width);
            read_width = true;
        }
        else if (KHeightTypeString == str_buff) {
            if (read_height)
                Warning(KRedefinedWarning, "ParseImageSpecs", KHeightTypeString);

            ReadAssignment(file);
            ReadInteger(file, height);

            read_height = true;
        }
        else if (KBlockCloseString == str_buff) {
            if (read_width && read_height)
                return;
            else
                Error(KIncompleteTypeError, "ParseImageSpec", "Incomplete Image Spec");
        } else {
            Error(KSyntaxError, "ParseImageSpec", str_buff);
        }
    }
}

Camera* InputFileReader::ParseCamera(std::ifstream &file) const
{
    char str_buff[100];

    ReadOpenBlock(file);

    int width, height;
    bool seen_width = false;
    bool seen_height = false;

    float fov;
    bool seen_fov = false;

    Math::vect_t eye_pos;
	INIT_VECTOR(eye_pos);
    bool seen_eye_pos = false;

    Math::vect_t look_vect;
	INIT_VECTOR(look_vect);
    bool seen_look_vect = false;

    Math::vect_t up_vector;
	INIT_VECTOR(up_vector);
    bool seen_up_vect = false;

    while (true) {
        ReadToken(file, str_buff);
        if (KWidthTypeString == str_buff) {
            if (seen_width)
                Warning(KRedefinedWarning,  "ParseCamera",KWidthTypeString);

            ReadAssignment(file);
            ReadInteger(file, width);
            seen_width = true;
        }
        else if (KHeightTypeString == str_buff) {
            if (seen_height)
                Warning(KRedefinedWarning,  "ParseCamera", KHeightTypeString);

            ReadAssignment(file);
            ReadInteger(file, height);
            seen_height = true;
        }
        else if (KFOVTypeString == str_buff) {
            if (seen_fov)
                Warning(KRedefinedWarning,  "ParseCamera", KFOVTypeString);

            ReadAssignment(file);
            ReadFloat(file, fov);
            seen_fov = true;
        }
        else if (KEyePositionTypeString == str_buff) {
            if (seen_eye_pos)
                Warning(KRedefinedWarning,  "ParseCamera", KEyePositionTypeString);

            ParseVector(file, eye_pos);
            seen_eye_pos = true;
        }
        else if (KLookVectorTypeString == str_buff) {
            if (seen_look_vect)
                Warning(KRedefinedWarning, "ParseCamera", KLookVectorTypeString);

            ParseVector(file, look_vect);
            seen_look_vect = true;
        }
        else if (KUpVectorTypeString == str_buff)  {
            if (seen_up_vect)
                Warning(KRedefinedWarning, "ParseCamera", KUpVectorTypeString);

            ParseVector(file, up_vector);
            seen_up_vect = true;
        }
        else if (KBlockCloseString == str_buff) {
            if (seen_up_vect && seen_look_vect && seen_eye_pos
                    && seen_fov && seen_width && seen_height) {
                Camera* cam = new Camera(width, height, fov,
                                        eye_pos[0], eye_pos[1], eye_pos[2],
                                        look_vect[0], look_vect[1], look_vect[2],
                                        up_vector[0], up_vector[1], up_vector[2]);

                return cam;
            } else {
                Error(KIncompleteTypeError, "ParseCamera", KCameraTypeString);
            }

        } else {
            Error(KSyntaxError, "ParseCamera",str_buff);
        }
    }
}


Sphere* InputFileReader::ParseSphere(std::ifstream &file) const
{

    ReadOpenBlock(file);

    char str_buff[100];
    float radius;
    bool seen_radius = false;

    Math::vect_t center;
	INIT_VECTOR(center);
    bool seen_center = false;

    Material* material = nullptr;
    bool seen_material = false;

    while (true) {
        ReadToken(file, str_buff);

        if (KRadiusTypeString == str_buff) {
            if (seen_radius)
                Warning(KRedefinedWarning, "ParseSphere", KRadiusTypeString);

            ReadAssignment(file);
            ReadFloat(file, radius);
            seen_radius = true;
        }
        else if (KCenterPointTypeString == str_buff) {
            if (seen_center)
                Warning(KRedefinedWarning, "ParseSphere", str_buff);

            ReadAssignment(file);
            ParsePoint(file, center);
            seen_center = true;
        }
        else if (KMaterialTypeString == str_buff) {
            if (seen_material)
                Warning(KRedefinedWarning, "ParseSphere", KMaterialTypeString);

            material = ParseMaterial(file);
            seen_material = true;
        }
        else if (KBlockCloseString == str_buff) {
            if (seen_radius && seen_center && seen_material) {
                Sphere* sphere = new Sphere(center, radius, *material);
                delete material;
                return sphere;
            } else {
                Error(KIncompleteTypeError, "ParseSphere", KSphereTypeString);
            }
        } else {
            Error(KSyntaxError, "ParseSphere", str_buff);
        }
    }
}


Triangle* InputFileReader::ParseTriangle(std::ifstream &file) const
{
    ReadOpenBlock(file);
    char str_buff[100];

    Math::vect_t p1, p2, p3;
	INIT_VECTOR(p1);
	INIT_VECTOR(p2);
	INIT_VECTOR(p3);
	
    bool seen_p1, seen_p2, seen_p3;
    seen_p1 = seen_p2 = seen_p3 = false;

    Material* material = nullptr;
    bool seen_material = false;

    while (true) {
        ReadToken(file, str_buff);
        if (KPointOneTypeString == str_buff) {
            if (seen_p1)
                Warning(KRedefinedWarning, "ParseTriangle", KPointOneTypeString);

            ReadAssignment(file);
            ParsePoint(file, p1);
            seen_p1 = true;
        }
        else if (KPointTwoTypeString == str_buff) {
            if (seen_p2)
                Warning(KRedefinedWarning, "ParseTriangle", KPointTwoTypeString);

            ReadAssignment(file);
            ParsePoint(file, p2);
            seen_p2 = true;
        }
        else if (KPointThreeTypeString == str_buff) {
            if (seen_p3)
                Warning(KRedefinedWarning, "ParseTriangle", KPointThreeTypeString);

            ReadAssignment(file);
            ParsePoint(file, p3);
            seen_p3 = true;
        }
        else if (KMaterialTypeString == str_buff) {
            if (seen_material)
                Warning(KRedefinedWarning, "ParseTriangle", KMaterialTypeString);

            material = ParseMaterial(file);
            seen_material = true;
        }
        else if (KBlockCloseString == str_buff) {
            if (seen_p1 && seen_p2 && seen_p3 && seen_material) {

                Triangle* tri = new Triangle(p1, p2, p3, *material);

                delete material;

                return tri;
            } else {
                Error(KIncompleteTypeError, "ParseTriangle",KTriangleTypeString);
            }
        } else {
            Error(KSyntaxError, "ParseTriangle", str_buff);
        }
    }
}



Quad* InputFileReader::ParseQuad(std::ifstream &file) const
{
    ReadOpenBlock(file);
    char str_buff[100];

    Math::vect_t p1, p2, p3, p4;
    bool seen_p1, seen_p2, seen_p3, seen_p4;
    seen_p1 = seen_p2 = seen_p3 = seen_p4 = false;

    Material* material = nullptr;
    bool seen_material = false;

    while (true) {
        ReadToken(file, str_buff);
        if (KPointOneTypeString == str_buff) {
            if (seen_p1)
                Warning(KRedefinedWarning, "ParseQuad", KPointOneTypeString);

            ReadAssignment(file);
            ParsePoint(file, p1);
            seen_p1 = true;
        }
        else if (KPointTwoTypeString == str_buff) {
            if (seen_p2)
                Warning(KRedefinedWarning, "ParseQuad", KPointTwoTypeString);
            ReadAssignment(file);
            ParsePoint(file, p2);
            seen_p2 = true;
        }
        else if (KPointThreeTypeString == str_buff) {
            if (seen_p3)
                Warning(KRedefinedWarning, "ParseQuad", KPointThreeTypeString);
            ReadAssignment(file);
            ParsePoint(file, p3);
            seen_p3 = true;
        }
        else if (KPointFourTypeString == str_buff) {
            if (seen_p4)
                Warning(KRedefinedWarning, "ParseQuad", KPointFourTypeString);

            ReadAssignment(file);
            ParsePoint(file, p4);
            seen_p4 = true;
        }
        else if (KMaterialTypeString == str_buff) {
            if (seen_material)
                Warning(KRedefinedWarning, "ParseQuad", KMaterialTypeString);

            material = ParseMaterial(file);
            seen_material = true;
        }
        else if (KBlockCloseString == str_buff) {
            if (seen_p1 && seen_p2 && seen_p3 && seen_material) {

                Quad* quad = new Quad(p1, p2, p3, p4, *material);

                delete material;

                return quad;
            } else {
                Error(KIncompleteTypeError, "ParseQuad", KQuadTypeString);
            }
        } else {
            Error(KSyntaxError, "ParseQuad", str_buff);
        }
    }
}


Light* InputFileReader::ParseLight(std::ifstream &file) const
{
    ReadOpenBlock(file);
    char str_buff[100];

    Math::vect_t location;
	INIT_VECTOR(location);
    bool seen_location = false;

    Color* diffuse_intensity = nullptr;
    bool seen_diffuse_intensity = false;

    Color* specular_intensity = nullptr;
    bool seen_specular_intensity = false;

    while (true) {
        ReadToken(file, str_buff);
        if (str_buff == KCenterPointTypeString) {
            if (seen_location)
                Warning(KRedefinedWarning, "ParseLight", KCenterPointTypeString);

            ReadAssignment(file);
            ParsePoint(file, location);
            seen_location = true;
        }
        else if (KDiffuseColorTypeString == str_buff) {
            if (seen_diffuse_intensity)
                Warning(KRedefinedWarning, "ParseLight", KDiffuseColorTypeString);

            diffuse_intensity = ParseColor(file);
            seen_diffuse_intensity = true;
        }
        else if (KSpecularColorTypeString == str_buff) {
            if (seen_specular_intensity)
                Warning(KRedefinedWarning, "ParseLight", KSpecularColorTypeString);

            specular_intensity = ParseColor(file);
            seen_specular_intensity = true;
        }
        else if (KBlockCloseString == str_buff) {
            if (seen_location && seen_diffuse_intensity && seen_specular_intensity) {
                Light* temp_light = new Light(*diffuse_intensity, *specular_intensity, location);
                delete diffuse_intensity;
                delete specular_intensity;

                return temp_light;
            } else {
                Error(KIncompleteTypeError, "ParseLight",KLightTypeString);
            }
        } else {
            Error(KSyntaxError, "ParseLight",str_buff);
        }
    }

}


Material* InputFileReader::ParseMaterial(std::ifstream &file) const
{
    ReadOpenBlock(file);
    char str_buff[100];

    Material* material = new Material();

    bool seen_ambient_color, seen_diffuse_color, seen_specular_color;
    bool seen_reflected_color, seen_refracted_color, seen_shine;

    seen_ambient_color = seen_diffuse_color = seen_specular_color = false;
    seen_reflected_color = seen_refracted_color = seen_shine = false;

    while (true) {
        ReadToken(file, str_buff);
        if (KAmbientColorTypeString == str_buff) {
            if (seen_ambient_color)
                Warning(KRedefinedWarning, "ParseMaterial", KAmbientColorTypeString);
            Color* temp_col = ParseColor(file);
            assert(temp_col);
            material->ambient_r_ = *temp_col;
            seen_ambient_color = true;
            delete temp_col;
        }
        else if (KDiffuseColorTypeString == str_buff) {
            if (seen_diffuse_color)
                Warning(KRedefinedWarning, "ParseMaterial", KDiffuseColorTypeString);
            Color* temp_col = ParseColor(file);
            assert(temp_col);
            material->diffuse_r_ = *temp_col;
            seen_diffuse_color = true;
            delete temp_col;
        }
        else if (KSpecularColorTypeString == str_buff) {
            if (seen_specular_color)
                Warning(KRedefinedWarning, "ParseMaterial", KSpecularColorTypeString);
            Color* temp_col = ParseColor(file);
            assert(temp_col);
            material->specular_r_ = *temp_col;
            seen_specular_color = true;
            delete temp_col;
        }
        else if (KReflectedColorTypeString == str_buff) {
            if (seen_reflected_color)
                Warning(KRedefinedWarning, "ParseMaterial", KReflectedColorTypeString);
            Color* temp_col = ParseColor(file);
            assert(temp_col);
            material->reflected_r_ = *temp_col;
            seen_reflected_color = true;
            delete temp_col;
        }
        else if (KRefractedColorTypeString == str_buff) {
            if (seen_refracted_color)
                Warning(KRedefinedWarning, "ParseMaterial", KRefractedColorTypeString);
            Color* temp_col = ParseColor(file);
            assert(temp_col);
            material->refracted_r_ = *temp_col;
            seen_refracted_color = true;
            delete temp_col;
        }
        else if (KShineTypeString == str_buff) {
            if (seen_shine)
                Warning(KRedefinedWarning, "ParseMaterial", KShineTypeString);

            int shine;
            ReadAssignment(file);
            ReadInteger(file, shine);
            material->shine_ = shine;
            seen_shine = true;
        }
        else if (KBlockCloseString == str_buff) {
            if (seen_ambient_color && seen_diffuse_color && seen_specular_color
                    && seen_reflected_color && seen_refracted_color && seen_shine) {
                return material;
            } else {
                Error(KIncompleteTypeError, "ParseMaterial", KMaterialTypeString);
            }
        } else {
            Error(KSyntaxError,"ParseMaterial", str_buff);
        }
    }
}

void InputFileReader::ParsePoint(std::ifstream &file, Math::vect_t& vect) const
{
    float x, y, z;
    bool success = true;
    success = success && ReadFloat(file, x);
    success = success && ReadFloat(file, y);
    success = success && ReadFloat(file, z);
    if (success) {
        vect[0] = x;
        vect[1] = y;
        vect[2] = z;
    }
    else
        Error(KIncompleteTypeError, "ParsePoint", KPointTypeString);
}

void InputFileReader::ParseVector(std::ifstream &file, Math::vect_t &vect) const
{
    ReadAssignment(file);
    float x, y, z;
    bool success = true;
    success = success && ReadFloat(file, x);
    success = success && ReadFloat(file, y);
    success = success && ReadFloat(file, z);
    if (success) {
        vect[0] = x;
        vect[1] = y;
        vect[2] = z;
    }
    else
        Error(KIncompleteTypeError, "ParseVector", KVectorTypeString);
}

Color* InputFileReader::ParseColor(std::ifstream &file) const
{
    ReadAssignment(file);
    float red, green, blue;
    bool success = true;
    success = success && ReadFloat(file, red);
    success = success && ReadFloat(file, green);
    success = success && ReadFloat(file, blue);
    if (success)
        return new Color(red, green, blue);
    else
        Error(KIncompleteTypeError, "ParseColor",KColorTypeString);
	return 0;
}

bool InputFileReader::ReadAssignment(std::ifstream &file) const
{
    char str_buff[50];
    ReadToken(file, str_buff);
    if (KAssignmentString != str_buff)
        Error(KSyntaxError, "ReadAssignment", str_buff);
    return true;
}

bool InputFileReader::ReadOpenBlock(std::ifstream &file) const
{
    char str_buff[50];
    ReadToken(file, str_buff);
    if (KBlockOpenString != str_buff)
        Error(KSyntaxError, "ReadOpenBlock",str_buff);
    return true;
}


bool InputFileReader::ReadInteger(std::ifstream &file, int &x) const
{
    if (!(file >> x) || !CheckFile(file))
        Error(KSyntaxError, "ReadInteger","Reading Integer");
    
    return true;
}

bool InputFileReader::ReadFloat(std::ifstream &file, float &x) const
{
    if (!(file >> x) || !CheckFile(file)) {
        Error(KSyntaxError, "ReadFloat", "Reading Float");
    }

    return true;
}

bool InputFileReader::ReadToken(std::ifstream &file, char *str_buff) const
{
    char temp_buff[100];
    int i, j;
    while (true) {
        if (!(file >> str_buff) || !CheckFile(file))
            Error(KSyntaxError, "ReadToken","Reading Token");
        
 
        j = 0;
        for (i=0; i < 100 && str_buff[i] != '\0'; ++i) {
            if (str_buff[i] > ' ') {
                temp_buff[j] = str_buff[i];
                ++j;
            }
        }
        if (j > 0) {
            for (i=0; i < j; ++i)
                str_buff[i] = temp_buff[i];
            if (j < 100)
                str_buff[j] = 0;
            else
                std::cout << "Buffer Overflow in Read Token" << std::endl;
            break;
        }

    }
	return true;
}

bool InputFileReader::CheckFile(std::ifstream &file) const
{
    return (file.is_open() && file.good());
}
