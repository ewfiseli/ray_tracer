/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_COLOR_H
#define CORE_COLOR_H

#include "math/mathdefs.h"
#include "commondef.h"

#define CLAMP(X) 	if (X < 0.0f) X = 0.0f; \
					if (X > 1.0f) X = 1.0f

class Color {
public:
    Color() {red_ = blue_ = green_ = 0;}

    Color(float red, float green, float blue) {
        red_ = red;
        green_ = green;
        blue_ = blue;
    }

    Color(const Color& col) {*this = col;}

    Color& operator =(const Color& col) {
        red_ = col.red_;
        blue_ = col.blue_;
        green_ = col.green_;
        return *this;
    }

    bool operator> (float val) {
        return (red_ > val || green_ > val || blue_ > val);
    }

    Color operator* (float factor) const {
        Color temp;
        temp.red_ = red_*factor;
        temp.blue_ = blue_*factor;
        temp.green_ = green_*factor;
        return temp;
    }

    Color& operator*= (float factor) {
        red_ *= factor;
        blue_*= factor;
        green_ *= factor;
        return *this;
    }

    Color operator* (const Color& col) const {
        Color temp_col;
        temp_col.red_ = red_ * col.red_;
        temp_col.blue_ = blue_ * col.blue_;
        temp_col.green_ = green_ * col.green_;
        return temp_col;
    }

    Color operator+ (const Color &col) const {
        Color temp;
        temp.blue_ = blue_ + col.blue_;
        temp.red_ = red_ + col.red_;
        temp.green_ = green_ + col.green_;
        return temp;
    }

    Color& operator+= (const Color &col) {

        red_ += col.red_;
        blue_ += col.blue_;
        green_ += col.green_;
        return *this;
    }

    void Clamp() {
        CLAMP(red_);
        CLAMP(blue_);
        CLAMP(green_);
    }


    float red_, blue_, green_;
};

#endif /* CORE_COLOR_H */