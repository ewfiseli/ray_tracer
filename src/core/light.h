/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_LIGHT_H
#define CORE_LIGHT_H

#include "core/color.h"
#include "math/vector3d.h"

class Light
{
public:
    Light(const Color &diffuse_intensity, const Color& specular_intensity, 
		  const Math::vect_t &location)
    {
        diffuse_intensity_ = diffuse_intensity;
        specular_intensity_ = specular_intensity;
        Math::Copy(location_, location);
    }

	Light& operator= (const Light& l)
    {
        diffuse_intensity_ = l.diffuse_intensity_;
        specular_intensity_ = l.specular_intensity_;
        Math::Copy(location_, l.location_);
        return (*this);
    }

    Color diffuse_intensity_;
    Color specular_intensity_;
    Math::vect_t location_;
};

#endif // LIGHT_H