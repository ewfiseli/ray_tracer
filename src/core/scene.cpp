/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "core/scene.h"
#include "core/light.h"
#include "core/ray.h"
#include "core/intersection.h"
#include "objects/object.h"

#include <cassert>

Scene::~Scene()
{
    int num_light = lights_.size();
    for (int i=0; i < num_light; ++i)
        delete lights_[i];
	for (auto it=objects_.begin(); it != objects_.end(); ++it) {
		delete *it;
	}
}

void Scene::AddObject(Object *object)
{
    assert(object);
#ifdef CONFIG_USE_RTREE
	tree_.Insert(object);
#endif
	objects_.push_back(object);
}

void Scene::AddLight(Light *light)
{
    lights_.push_back(light);
}

Intersection* Scene::FindIntersect(const Ray &ray) const
{
	
#if defined(CONFIG_USE_RTREE)
	return tree_.Search(ray);
#else
	Intersection* intersect = nullptr;
	Intersection* temp_intersection = nullptr;

	int num_obj = objects_.size();
	for (int i=0; i < num_obj; ++i) {
		temp_intersection = objects_[i]->Intersect(ray);
		if (temp_intersection) {
			if (!intersect) {
				intersect = temp_intersection;
			}
			else if (temp_intersection->dist_ < intersect->dist_) {
				delete intersect;
				intersect = temp_intersection;
			} else {
				delete temp_intersection;
			}
		}
	}
	return intersect;
#endif 
}

Intersection* Scene::FindIntersect(const Ray &ray, float max) const
{
#if defined(CONFIG_USE_RTREE)
	 return tree_.Search(ray, max);
#else
	 Intersection *tmp = FindIntersect(ray);
	 if (tmp && tmp->dist_ > max) {
		 delete tmp;
		 tmp = nullptr;
	 }
	 return tmp;
#endif
}