/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_SCENE_H
#define CORE_SCENE_H

#include "core/color.h"
#include "math/vector3d.h"
#include <vector>

#if defined(CONFIG_USE_RTREE)
#include "rtree/rtree.h"
#endif

class Object;
class Intersection;
class Light;
class Ray;

class Scene
{
public:
    Scene() {};
    ~Scene();

    void AddObject(Object* object);
    void AddLight(Light* light);
    Intersection* FindIntersect(const Ray &ray) const;
    Intersection* FindIntersect(const Ray &ray, float max) const;

    std::vector<Light*> lights_;
    Math::vect_t camera_pos_;
    Color ambient_light_;
    Color backgroud_light_;
#ifdef CONFIG_USE_RTREE
	/* Rtree does not delete objects */
    RTree tree_;
	/* store alternate copy */
#endif
    std::vector<Object*> objects_;

private:
	DISALLOW_COPY_AND_ASSIGN(Scene);
};

#endif /* CORE_SCENE_H */