/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_INTERSECTION_H
#define CORE_INTERSECTION_H

#include "math/vector3d.h"

class Material;

class Intersection
{
public:
    Intersection() {}
    Math::vect_t normal_;
    Math::vect_t intersect_point_;
    float dist_;
    const Material *mat_;
    bool found;
private:
	DISALLOW_COPY_AND_ASSIGN(Intersection);
};

#endif /* CORE_INTERSECTION_H */