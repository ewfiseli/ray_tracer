/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_IMAGE_H
#define CORE_IMAGE_H

#include "core/color.h"

class Image {
public:
    Image(int numRows, int numCols);
	
    ~Image();
	
    void GetPixel(int row, int col, unsigned char &r,
				  unsigned char &g, unsigned char &b);

    void SetPixel(int row, int col, float r, float g, float b);

    static unsigned char FloatToUChar(float f);

    void SaveImage(const char* file_name);
	
    int NumRows() {return numRows_;}
    int NumCols() {return numCols_;}
    
private:
    unsigned char* image_;
    int numRows_;
    int numCols_;
	DISALLOW_COPY_AND_ASSIGN(Image);
};

#endif /* CORE_IMAGE_H */