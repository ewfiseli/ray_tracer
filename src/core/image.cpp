/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "core/image.h"
#include "core/rgbimage.h"

#include <sstream>
#include <algorithm>
#include <cmath>

#define NUM_CHANNELS 255
#define IMAGE_SUFFIX ".out"

Image::Image(int numRows, int numCols)
{
    image_ = new unsigned char[numRows*numCols*3];
    numRows_ = numRows;
    numCols_ = numCols;
}

Image::~Image()
{
    delete [] image_;
}

void Image::GetPixel(int row, int col, unsigned char &r, 
					 unsigned char &g, unsigned char &b)
{
    int offset = (row*numCols_ + col) * 3;
    r = image_[offset];
    g = image_[offset + 1];
    b = image_[offset + 2];
    return;
}

void Image::SetPixel(int row, int col, float r, float g, float b)
{
    int offset = (row*numCols_ + col) * 3;
    image_[offset] = Image::FloatToUChar(r);
    image_[offset + 1] = Image::FloatToUChar(g);
    image_[offset + 2] = Image::FloatToUChar(b);
    return;
}

void Image::SaveImage(const char* file_name)
{
    RgbImage new_image(numRows_, numCols_);
    unsigned char red, green, blue;

    for (int i=0; i < numRows_; ++i) {
        for (int j=0; j < numCols_; ++j) {
            GetPixel(i, j, red, green, blue);
            new_image.SetRgbPixelc(numRows_ - i - 1, j, red, green, blue);
        }
    }
    std::stringstream ss;
    ss << file_name << IMAGE_SUFFIX;
    new_image.WriteBmpFile(ss.str().c_str());
}

unsigned char Image::FloatToUChar(float f)
{
    double channel_width = 1.0f / NUM_CHANNELS;
    /* clamp */
    f = std::max(0.0f, std::min(1.0f, f));
    return floor(f / channel_width);
}