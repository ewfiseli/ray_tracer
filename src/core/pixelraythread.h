/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_PIXELRAYTHREAD_H
#define CORE_PIXELRAYTHREAD_H

#include "core/ray.h"
#include "math/vector3d.h"

#include <vector>

/* set recursive max depth, default is 3 if not defined in build */
#ifndef CONFIG_REC_DEPTH
#define REC_DEPTH 3
#else
#define REC_DEPTH CONFIG_REC_DEPTH
#endif

class Scene;
class Camera;
class Image;
class Light;
class Intersection;

class PixelRayThread 
{
public:
    explicit PixelRayThread(Scene const* scene, Camera const* camera,
                            Image* image);
	
    void Insert(int row, int col);
    ~PixelRayThread() { } 
    
    void run();
private:
	
	typedef std::pair<int, int> pixel_pair_t;
	
    void RunTraceRay();

    Color* TraceRay(Ray &ray, int depth);

    inline void CalculateIllumination(Color& dest_color, 
									  const Light& light, 
									  const Ray& to_light, 
									  const Intersection& intersection);

    inline void CalculateReflection(Math::vect_t& reflect, 
									const Math::vect_t& normal, 
									const Math::vect_t& incident, bool reversed);

    inline void CalculateColorForHit(Color& local_color, 
									 const Intersection& obj_intersection);

    inline void GetPointOffsets(Math::vect_t points[5], 
								const Math::vect_t& orthog_vect_1, 
								const Math::vect_t& orthog_vect_2);

    static const int MAX_DEPTH = REC_DEPTH;

    /* Reused objects for calculations */
	Ray starting_ray_;
    Math::vect_t to_camera_;
    Math::vect_t reflection_;
    Ray to_light_;
	
	
    int pixel_row_, pixel_col_;
	const Scene *scene_;
    const Camera *camera_;
	Image* image_;
    std::vector<pixel_pair_t> pixels_;

#if defined(CONFIG_TRACE_5)
    Ray ray_copy;
    Color temp_color;
    Math::vect_t points_[5];
#endif

#if defined(CONFIG_SHADOW_5)
    Color temp_light_color;
    Math::vect_t light_points_[5];
    Math::vect_t temp_v, orthog_1, orthog_2;
#endif
	
	DISALLOW_COPY_AND_ASSIGN(PixelRayThread);
};


#endif /* CORE_PIXELRAYTHREAD_H */