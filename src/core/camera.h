/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_CAMERA_H
#define CORE_CAMERA_H

#include "math/vector3d.h"

class Ray;

class Camera
{
public:
    Camera(int width, int height, float fov,
            int x_pos, int y_pos, int z_pos,
			int look_x, int look_y, int look_z,
			float up_x, float up_y, float up_z);

    void GetRay(Ray& ray, int x, int y) const;
	
    int width_, height_;
    float fov_, f1_;
    Math::vect_t eye_, look_, up_;
    Math::vect_t Du_, Dv_, Vp_;
#ifdef CONFIG_TRACE_5
    Math::vect_t Du_trace_5_, Dv_trace_5_;
#endif
private:
	DISALLOW_COPY_AND_ASSIGN(Camera);
};

#endif /* CORE_CAMERA_H */