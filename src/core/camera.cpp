/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "core/camera.h"
#include "core/ray.h"
#include "math/mathdefs.h"
#include "math/vector3d.h"

#include <cmath>

#define TRACE_5_MOVEMENT 0.10

Camera::Camera(int width, int height, float fov,
               int x_pos, int y_pos, int z_pos,
               int look_x, int look_y, int look_z,
               float up_x, float up_y, float up_z)
{
    width_ = width;
    height_ = height;
    fov_ = fov;

    eye_[0] = x_pos;
    eye_[1] = y_pos;
    eye_[2] = z_pos;

    look_[0] = look_x-x_pos;
    look_[1] = look_y-y_pos;
    look_[2] = look_z-z_pos;

    up_[0] = up_x;
    up_[1] = up_y;
    up_[2] = up_z;


    Math::CrossProduct(Du_, look_, up_);
    Math::Normalize(Du_);


    Math::CrossProduct(Dv_, look_, Du_);
    Math::Normalize(Dv_);
	
#ifdef CONFIG_TRACE_5
	Math::MultiplyVal(Du_trace_5_, Du_, TRACE_5_MOVEMENT);
    Math::MultiplyVal(Dv_trace_5_, Dv_, TRACE_5_MOVEMENT);
#endif

    f1_ = (width_ / (2*tan((0.5*fov_)*PI/180)));

    Math::Copy(Vp_, look_);
    Math::Normalize(Vp_);

    Vp_[0] = Vp_[0]*f1_ -0.5*(width_*Du_[0] + height_*Dv_[0]);
    Vp_[1] = Vp_[1]*f1_ -0.5*(width_*Du_[1] + height_*Dv_[1]);
    Vp_[2] = Vp_[2]*f1_ -0.5*(width_*Du_[2] + height_*Dv_[2]);
}

void Camera::GetRay(Ray& ray, int x, int y) const
{
    Math::Copy(ray.base_, eye_);
    ray.direction_[0] = x*Du_[0] + y*Dv_[0] + Vp_[0];
    ray.direction_[1] = x*Du_[1] + y*Dv_[1] + Vp_[1];
    ray.direction_[2] = x*Du_[2] + y*Dv_[2] + Vp_[2];

    ray.SetIntensityFull();
    ray.Recalculate();
}