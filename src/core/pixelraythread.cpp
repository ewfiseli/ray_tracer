/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "core/pixelraythread.h"
#include "core/image.h"
#include "core/scene.h"
#include "core/light.h"
#include "core/intersection.h"
#include "core/camera.h"
#include "math/mathdefs.h"
#include "objects/object.h"

#include <cassert>
#include <vector>
#include <utility>

PixelRayThread::PixelRayThread(const Scene *scene, const Camera *camera, 
							  Image *image) : 
							  scene_(scene), camera_(camera), image_(image)
{ }

void PixelRayThread::run()
{
    while(pixels_.size() > 0) {
        auto temp = pixels_.back();
        pixels_.pop_back();
        pixel_row_ = std::get<0>(temp);
        pixel_col_ = std::get<1>(temp);
        camera_->GetRay(starting_ray_, pixel_col_, pixel_row_);
        RunTraceRay();
    }
}

void PixelRayThread::Insert(int row, int col)
{
	auto temp_pixel = std::make_pair(row, col);
    pixels_.push_back(temp_pixel);
}

void PixelRayThread::RunTraceRay()
{
#if defined(CONFIG_TRACE_5)

   Ray::CopyRay(ray_copy, starting_ray_);
   Math::Copy(points_[0], ray_copy.base_);
   GetPointOffsets(points_, camera_->Du_trace_5_, camera_->Dv_trace_5_);

   temp_color.red_ = 0;
   temp_color.blue_ = 0;
   temp_color.green_ = 0;

   Color *return_col = TraceRay(starting_ray_, 0);
   temp_color += (*return_col);
   delete return_col;
   for (int i=1; i < 5; ++i) {
       Ray::CopyRay(starting_ray_, ray_copy);
       Math::Copy(starting_ray_.base_, points_[i]);
       return_col = TraceRay(starting_ray_, 0);
       temp_color += (*return_col);
       delete return_col;
   }
   
   temp_color.red_ /= 5.0f;
   temp_color.blue_ /= 5.0f;
   temp_color.green_ /= 5.0f;
   image_->SetPixel(pixel_row_, pixel_col_, temp_color.red_,
                                            temp_color.green_,
                                            temp_color.blue_);

#else
       Color *ret_color = TraceRay(starting_ray_, 0);
       image_->SetPixel(pixel_row_, pixel_col_, ret_color->red_,
                                                ret_color->green_,
                                                ret_color->blue_);
       delete ret_color;
#endif

}


inline void PixelRayThread::GetPointOffsets(Math::vect_t points[5], 
											const Math::vect_t &orthog_vect_1,
											const Math::vect_t& orthog_vect_2)
{
    Math::AddVector(points[1], points[0], orthog_vect_1);
    Math::SubtractVector(points[2], points[0], orthog_vect_1);

    Math::AddVector(points[3], points[0], orthog_vect_2);
    Math::SubtractVector(points[4], points[0], orthog_vect_2);
}

inline void PixelRayThread::CalculateReflection(Math::vect_t& reflect, 
											 const Math::vect_t& normal, 
											 const Math::vect_t& incident, 
											 bool reversed)
{
    float dot_prod = 2.0f * Math::DotProduct(incident, normal);
    Math::MultiplyVal(reflect, normal, dot_prod);

    if (reversed)
        Math::SubtractVector(reflect, reflect, incident);
    else
        Math::SubtractVector(reflect, incident, reflect);

    Math::Normalize(reflect);
}

inline void PixelRayThread::CalculateIllumination(Color& dest_color, 
												const Light &light, 
												const Ray& to_light,
												const Intersection& intersection)
{
    float dot_prod = Math::DotProduct(to_light.direction_, 
											intersection.normal_);
    assert(dot_prod == dot_prod);
    if ((dot_prod != dot_prod) || dot_prod <= 0)
        return;

    dest_color += (light.diffuse_intensity_ * 
				  (intersection.mat_->diffuse_r_ * dot_prod));

    /* Get reflection of light */
    CalculateReflection(reflection_, intersection.normal_, 
						to_light.direction_, true);

    /* get ray to camera */
    Math::SubtractVector(to_camera_, 
						 scene_->camera_pos_,intersection.intersect_point_);
    Math::Normalize(to_camera_);

    dot_prod = Math::DotProduct(reflection_, to_camera_);

    if ((dot_prod != dot_prod) || dot_prod <= 0)
        return;


    dot_prod = pow(dot_prod, intersection.mat_->shine_);
    dest_color += (light.specular_intensity_  * 
					(intersection.mat_->specular_r_ * dot_prod));

}

void PixelRayThread::CalculateColorForHit(Color& local_color,
										  const Intersection& obj_intersection) 
{
    Light* temp_light;
    Intersection* intersection;
	
#if defined(CONFIG_SHADOW_5)
	float diff = 2.0f;
    int num_lights = scene_->lights_.size();
    for (int i=0; i < num_lights; ++i) {
        temp_light = scene_->lights_[i];

        /* Create variables for ray to light */
        Math::SubtractVector(to_light_.direction_, temp_light->location_, 
							 obj_intersection.intersect_point_);
        Math::Copy(to_light_.base_, obj_intersection.intersect_point_);

        /* Get distance to light */
        float dist_to_light = Math::Length(to_light_.direction_);

        /* Normalize and recalculate sections of ray */
        to_light_.Recalculate();

        /* Get Orthogonal Vectors */
        Math::GetOrthogVector(orthog_1, to_light_.direction_);
        Math::Normalize(orthog_1);
        Math::MultiplyVal(orthog_1, diff);

        Math::CrossProduct(orthog_2, to_light_.direction_, orthog_1);
        Math::Normalize(orthog_2);
        Math::MultiplyVal(orthog_2, diff);

        /* Calculate Light Point offsets */
        Math::Copy(light_points_[0], temp_light->location_);
        GetPointOffsets(light_points_, orthog_1, orthog_2);

        temp_light_color.red_ = 0;
        temp_light_color.blue_ = 0;
        temp_light_color.green_ = 0;
        /* Shoot each shadow Ray */
        for (int j=0; j < 5; ++j) {
            intersection = scene_->FindIntersect(to_light_, dist_to_light);

            if (!intersection) {
                CalculateIllumination(temp_light_color, *temp_light, 
									  to_light_, obj_intersection);
            } else {
				delete intersection;
				intersection = nullptr;
            }
			
            if (j != 4) {
                Math::SubtractVector(to_light_.direction_, light_points_[j+1], 
									 obj_intersection.intersect_point_);
                dist_to_light = Math::Length(to_light_.direction_);
                assert(dist_to_light == dist_to_light);
                to_light_.Recalculate();
            }
        }

        /* divide intensity by number of rays traced */
        temp_light_color.red_ /= 5.0;
        temp_light_color.green_ /= 5.0;
        temp_light_color.blue_ /= 5.0;
        local_color += temp_light_color;
        local_color.Clamp();
    }
    
#else /* Only trace one shadow */

	int num_lights = scene_->lights_.size();
    for (int i=0; i < num_lights; ++i) {
        temp_light = scene_->lights_[i];

        /* Create variables for ray to light */
        Math::SubtractVector(to_light_.direction_, temp_light->location_, 
							 obj_intersection.intersect_point_);
        Math::Copy(to_light_.base_, obj_intersection.intersect_point_);

        /* Get distance to light */
        float dist_to_light = Math::Length(to_light_.direction_);

        /* Normalize and recalculate sections of ray */
        to_light_.Recalculate();

        /* Create a ray and see if there is an intersection 
		 * between the start and the light */
        intersection = scene_->FindIntersect(to_light_, dist_to_light);

        if(!intersection) {
            CalculateIllumination(local_color, *temp_light, to_light_, 
								 obj_intersection);

        } else {
            delete intersection;
        }
    }
    
#endif	/* CONFIG_SHADOW_5 */
}


Color* PixelRayThread::TraceRay(Ray &ray, int depth)
{
    if (depth > MAX_DEPTH)
        return new Color(scene_->backgroud_light_);

    Color* local_color = new Color();
    Intersection *intersection = scene_->FindIntersect(ray);

    if (intersection) {
        /* calculate ambiant specular and difuse lighting */
        *local_color = intersection->mat_->ambient_r_ * scene_->ambient_light_;
        CalculateColorForHit(*local_color, *intersection);

        /* Calculate Reflected Ray */
        ray.intensity_ = ray.intensity_ * intersection->mat_->reflected_r_;

        if (ray.intensity_ > 0.01) {
            CalculateReflection(reflection_, intersection->normal_,
								ray.direction_, false);

            Math::Copy(ray.direction_, reflection_);
            Math::Copy(ray.base_, intersection->intersect_point_);
            ray.Recalculate();

            /* Recursivly trace reflected ray and add result */
            Color* reflected_color = TraceRay(ray, depth + 1);
            (*local_color) += ( (*reflected_color) * 
								intersection->mat_->reflected_r_);

            delete reflected_color;
        }
        
        delete intersection;
    }
    
    return local_color;
}
