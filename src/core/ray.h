/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of rtracer.
 *
 * rtracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rtracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rtracer.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORE_RAY_H
#define CORE_RAY_H

#include "core/color.h"
#include "math/vector3d.h"

class Ray
{
public:
    Ray() { }
	~Ray() { }

    inline void Recalculate()
    {

        this->Normalize();
        inv_direction_[0] = 1.0 / direction_[0];
        inv_direction_[1] = 1.0 / direction_[1];
        inv_direction_[2] = 1.0 / direction_[2];

        sign_[0] = (inv_direction_[0] < 0);
        sign_[1] = (inv_direction_[1] < 0);
        sign_[2] = (inv_direction_[2] < 0);

    }

    inline void Normalize()
    {
        Math::Normalize(direction_);
    }

    inline void SetIntensityFull()
    {
        intensity_.red_ = 1.0;
        intensity_.green_ = 1.0;
        intensity_.blue_ = 1.0;
    }

    static inline void CopyRay(Ray& dest_ray, const Ray& ray)
    {
        Math::Copy(dest_ray.base_, ray.base_);
        Math::Copy(dest_ray.direction_, ray.direction_);
        Math::Copy(dest_ray.inv_direction_, ray.inv_direction_);
        dest_ray.intensity_ = ray.intensity_;
        dest_ray.sign_[0] = ray.sign_[0];
        dest_ray.sign_[1] = ray.sign_[1];
        dest_ray.sign_[2] = ray.sign_[2];
    }


    Color intensity_;
    Math::vect_t direction_;
    Math::vect_t base_;
    Math::vect_t inv_direction_;
    int sign_[3];
	
private:
	DISALLOW_COPY_AND_ASSIGN(Ray);
};


#endif /* CORE_RAY_H */