.PHONY: all
all:
	@ $(MAKE) --no-print-directory -C build
	
.PHONY: e
e:
	@ $(MAKE) --no-print-directory distclean
	@ $(MAKE) --no-print-directory redep
	@ $(MAKE) --no-print-directory -C build -j
	
.PHONY: redep
redep: 
	@ $(MAKE) --no-print-directory clean
	@ rm -rf build/
	@ mkdir -p build/ ; cd build/ ; cmake .. ; cd ..

.PHONY: clean
clean:
	@ if [ -f build/Makefile ]; then $(MAKE) --no-print-directory -C build clean ; fi
		
.PHONY: distclean
distclean:
	@ $(MAKE) --no-print-directory clean
	@ rm -rf build/ 
	@ rm -f tests/*.out
	@ rm -f rtracer
	

